RocketMQ 自带一个 mqadmin 管理工具，本文档对该工具所有命令进行梳理

```
[yangzhijia@localhost bin]$ sh mqadmin
The most commonly used mqadmin commands are:
   updateTopic          Update or create topic
   deleteTopic          Delete topic from broker and NameServer.
   updateSubGroup       Update or create subscription group
   deleteSubGroup       Delete subscription group from broker.
   updateBrokerConfig   Update broker's config
   updateTopicPerm      Update topic perm
   topicRoute           Examine topic route info
   topicStatus          Examine topic Status info
   topicClusterList     get cluster info for topic
   brokerStatus         Fetch broker runtime status data
   queryMsgById         Query Message by Id
   queryMsgByKey        Query Message by Key
   queryMsgByUniqueKey  Query Message by Unique key
   queryMsgByOffset     Query Message by offset
   queryMsgByUniqueKey  Query Message by Unique key
   printMsg             Print Message Detail
   sendMsgStatus        send msg to broker.
   brokerConsumeStats   Fetch broker consume stats data
   producerConnection   Query producer's socket connection and client version
   consumerConnection   Query consumer's socket connection, client version and subscription
   consumerProgress     Query consumers's progress, speed
   consumerStatus       Query consumer's internal data structure
   cloneGroupOffset     clone offset from other group.
   clusterList          List all of clusters
   topicList            Fetch all topic list from name server
   updateKvConfig       Create or update KV config.
   deleteKvConfig       Delete KV config.
   wipeWritePerm        Wipe write perm of broker in all name server
   resetOffsetByTime    Reset consumer offset by timestamp(without client restart).
   updateOrderConf      Create or update or delete order conf
   cleanExpiredCQ       Clean expired ConsumeQueue on broker.
   cleanUnusedTopic     Clean unused topic on broker.
   startMonitoring      Start Monitoring
   statsAll             Topic and Consumer tps stats
   syncDocs             Synchronize wiki and issue to github.com
   allocateMQ           Allocate MQ
   checkMsgSendRT       check message send response time
   clusterRT            List All clusters Message Send RT

See 'mqadmin help <command>' for more information on a specific command.
```


```
sh mqadmin | awk 'BEGIN{ getline }{ print $1 }' | xargs -I {} sh -c  "sh mqadmin help  {}; echo -e '\n'"

usage: mqadmin updateTopic [-b <arg>] [-c <arg>] [-h] [-n <arg>] [-o <arg>] [-p <arg>] [-r <arg>] [-s <arg>]
       -t <arg> [-u <arg>] [-w <arg>]
 -b,--brokerAddr <arg>       create topic to which broker
 -c,--clusterName <arg>      create topic to which cluster
 -h,--help                   Print help
 -n,--namesrvAddr <arg>      Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -o,--order <arg>            set topic's order(true|false
 -p,--perm <arg>             set topic's permission(2|4|6), intro[2:W 4:R; 6:RW]
 -r,--readQueueNums <arg>    set read queue nums
 -s,--hasUnitSub <arg>       has unit sub (true|false
 -t,--topic <arg>            topic name
 -u,--unit <arg>             is unit topic (true|false
 -w,--writeQueueNums <arg>   set write queue nums


usage: mqadmin deleteTopic -c <arg> [-h] [-n <arg>] -t <arg>
 -c,--clusterName <arg>   delete topic from which cluster
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -t,--topic <arg>         topic name


usage: mqadmin updateSubGroup [-b <arg>] [-c <arg>] [-d <arg>] -g <arg> [-h] [-i <arg>] [-m <arg>] [-n <arg>]
       [-q <arg>] [-r <arg>] [-s <arg>] [-w <arg>]
 -b,--brokerAddr <arg>                     create subscription group to which broker
 -c,--clusterName <arg>                    create subscription group to which cluster
 -d,--consumeBroadcastEnable <arg>         broadcast
 -g,--groupName <arg>                      consumer group name
 -h,--help                                 Print help
 -i,--brokerId <arg>                       consumer from which broker id
 -m,--consumeFromMinEnable <arg>           from min offset
 -n,--namesrvAddr <arg>                    Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -q,--retryQueueNums <arg>                 retry queue nums
 -r,--retryMaxTimes <arg>                  retry max times
 -s,--consumeEnable <arg>                  consume enable
 -w,--whichBrokerWhenConsumeSlowly <arg>   which broker id when consume slowly


usage: mqadmin deleteSubGroup [-b <arg>] [-c <arg>] -g <arg> [-h] [-n <arg>]
 -b,--brokerAddr <arg>    delete subscription group from which broker
 -c,--clusterName <arg>   delete subscription group from which cluster
 -g,--groupName <arg>     subscription group name
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876


usage: mqadmin updateBrokerConfig [-b <arg>] [-c <arg>] [-h] -k <arg> [-n <arg>] -v <arg>
 -b,--brokerAddr <arg>    update which broker
 -c,--clusterName <arg>   update which cluster
 -h,--help                Print help
 -k,--key <arg>           config key
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -v,--value <arg>         config value


usage: mqadmin updateTopicPerm [-b <arg>] [-c <arg>] [-h] [-n <arg>] -p <arg> -t <arg>
 -b,--brokerAddr <arg>    create topic to which broker
 -c,--clusterName <arg>   create topic to which cluster
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -p,--perm <arg>          set topic's permission(2|4|6), intro[2:R; 4:W; 6:RW]
 -t,--topic <arg>         topic name


usage: mqadmin topicRoute [-h] [-n <arg>] -t <arg>
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -t,--topic <arg>         topic name


usage: mqadmin topicStatus [-h] [-n <arg>] -t <arg>
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -t,--topic <arg>         topic name


usage: mqadmin topicClusterList [-h] [-n <arg>] -t <arg>
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -t,--topic <arg>         topic name


usage: mqadmin brokerStatus -b <arg> [-h] [-n <arg>]
 -b,--brokerAddr <arg>    Broker address
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876


usage: mqadmin queryMsgById [-d <arg>] [-g <arg>] [-h] -i <arg> [-n <arg>]
 -d,--clientId <arg>        The consumer's client id
 -g,--consumerGroup <arg>   consumer group name
 -h,--help                  Print help
 -i,--msgId <arg>           Message Id
 -n,--namesrvAddr <arg>     Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876


usage: mqadmin queryMsgByKey [-h] -k <arg> [-n <arg>] -t <arg>
 -h,--help                Print help
 -k,--msgKey <arg>        Message Key
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -t,--topic <arg>         topic name


usage: mqadmin queryMsgByUniqueKey [-d <arg>] [-g <arg>] [-h] -i <arg> [-n <arg>] -t <arg>
 -d,--clientId <arg>        The consumer's client id
 -g,--consumerGroup <arg>   consumer group name
 -h,--help                  Print help
 -i,--msgId <arg>           Message Id
 -n,--namesrvAddr <arg>     Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -t,--topic <arg>           The topic of msg


usage: mqadmin queryMsgByOffset -b <arg> [-h] -i <arg> [-n <arg>] -o <arg> -t <arg>
 -b,--brokerName <arg>    Broker Name
 -h,--help                Print help
 -i,--queueId <arg>       Queue Id
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -o,--offset <arg>        Queue Offset
 -t,--topic <arg>         topic name


usage: mqadmin queryMsgByUniqueKey [-d <arg>] [-g <arg>] [-h] -i <arg> [-n <arg>] -t <arg>
 -d,--clientId <arg>        The consumer's client id
 -g,--consumerGroup <arg>   consumer group name
 -h,--help                  Print help
 -i,--msgId <arg>           Message Id
 -n,--namesrvAddr <arg>     Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -t,--topic <arg>           The topic of msg


usage: mqadmin printMsg [-b <arg>] [-c <arg>] [-d <arg>] [-e <arg>] [-h] [-n <arg>] [-s <arg>] -t <arg>
 -b,--beginTimestamp  <arg>   Begin timestamp[currentTimeMillis|yyyy-MM-dd#HH:mm:ss:SSS]
 -c,--charsetName  <arg>      CharsetName(eg: UTF-8、GBK)
 -d,--printBody  <arg>        print body
 -e,--endTimestamp  <arg>     End timestamp[currentTimeMillis|yyyy-MM-dd#HH:mm:ss:SSS]
 -h,--help                    Print help
 -n,--namesrvAddr <arg>       Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -s,--subExpression  <arg>    Subscribe Expression(eg: TagA || TagB)
 -t,--topic <arg>             topic name


usage: mqadmin sendMsgStatus -b <arg> [-c <arg>] [-h] [-n <arg>] [-s <arg>]
 -b,--brokerName <arg>    Broker Name
 -c,--count <arg>         send message count, Default: 50
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -s,--messageSize <arg>   Message Size, Default: 128


usage: mqadmin brokerConsumeStats -b <arg> [-h] [-l <arg>] [-n <arg>] [-o <arg>] [-t <arg>]
 -b,--brokerAddr <arg>      Broker address
 -h,--help                  Print help
 -l,--level <arg>           threshold of print diff
 -n,--namesrvAddr <arg>     Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -o,--order <arg>           order topic
 -t,--timeoutMillis <arg>   request timeout Millis


usage: mqadmin producerConnection -g <arg> [-h] [-n <arg>] -t <arg>
 -g,--producerGroup <arg>   producer group name
 -h,--help                  Print help
 -n,--namesrvAddr <arg>     Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -t,--topic <arg>           topic name


usage: mqadmin consumerConnection -g <arg> [-h] [-n <arg>]
 -g,--consumerGroup <arg>   consumer group name
 -h,--help                  Print help
 -n,--namesrvAddr <arg>     Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876


usage: mqadmin consumerProgress [-g <arg>] [-h] [-n <arg>]
 -g,--groupName <arg>     consumer group name
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876


usage: mqadmin consumerStatus -g <arg> [-h] [-i <arg>] [-n <arg>] [-s]
 -g,--consumerGroup <arg>   consumer group name
 -h,--help                  Print help
 -i,--clientId <arg>        The consumer's client id
 -n,--namesrvAddr <arg>     Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -s,--jstack                Run jstack command in the consumer progress


usage: mqadmin cloneGroupOffset -d <arg> [-h] [-n <arg>] [-o <arg>] -s <arg> -t <arg>
 -d,--destGroup <arg>     set destination consumer group
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -o,--offline <arg>       the group or the topic is offline
 -s,--srcGroup <arg>      set source consumer group
 -t,--topic <arg>         set the topic


usage: mqadmin clusterList [-h] [-i <arg>] [-m] [-n <arg>]
 -h,--help                Print help
 -i,--interval <arg>      specify intervals numbers, it is in seconds
 -m,--moreStats           Print more stats
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876


usage: mqadmin topicList [-c] [-h] [-n <arg>]
 -c,--clusterModel        clusterModel
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876


usage: mqadmin updateKvConfig [-h] -k <arg> [-n <arg>] -s <arg> -v <arg>
 -h,--help                Print help
 -k,--key <arg>           set the key name
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -s,--namespace <arg>     set the namespace
 -v,--value <arg>         set the key value


usage: mqadmin deleteKvConfig [-h] -k <arg> [-n <arg>] -s <arg>
 -h,--help                Print help
 -k,--key <arg>           set the key name
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -s,--namespace <arg>     set the namespace


usage: mqadmin wipeWritePerm -b <arg> [-h] [-n <arg>]
 -b,--brokerName <arg>    broker name
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876


usage: mqadmin resetOffsetByTime [-c] [-f <arg>] -g <arg> [-h] [-n <arg>] -s <arg> -t <arg>
 -c,--cplus               reset c++ client offset
 -f,--force <arg>         set the force rollback by timestamp switch[true|false]
 -g,--group <arg>         set the consumer group
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -s,--timestamp <arg>     set the timestamp[now|currentTimeMillis|yyyy-MM-dd#HH:mm:ss:SSS]
 -t,--topic <arg>         set the topic


usage: mqadmin updateOrderConf [-h] -m <arg> [-n <arg>] -t <arg> [-v <arg>]
 -h,--help                Print help
 -m,--method <arg>        option type [eg. put|get|delete
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -t,--topic <arg>         topic name
 -v,--orderConf <arg>     set order conf [eg. brokerName1:num;brokerName2:num]


usage: mqadmin cleanExpiredCQ [-b <arg>] [-c <arg>] [-h] [-n <arg>]
 -b,--brokerAddr <arg>    Broker address
 -c,--cluster <arg>       clustername
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876


usage: mqadmin cleanUnusedTopic [-b <arg>] [-c <arg>] [-h] [-n <arg>]
 -b,--brokerAddr <arg>    Broker address
 -c,--cluster <arg>       cluster name
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876


usage: mqadmin startMonitoring [-h] [-n <arg>]
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876


usage: mqadmin statsAll [-a] [-h] [-n <arg>]
 -a,--activeTopic         print active topic only
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876


usage: mqadmin syncDocs [-h] [-n <arg>] -p <arg> -u <arg>
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -p,--password <arg>      Password of github.com
 -u,--userName <arg>      User name of github.com


usage: mqadmin allocateMQ [-h] -i <arg> [-n <arg>] -t <arg>
 -h,--help                Print help
 -i,--ipList <arg>        ipList
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -t,--topic <arg>         topic name


usage: mqadmin checkMsgSendRT [-a <arg>] [-h] [-n <arg>] -s <arg> -t <arg>
 -a,--amout <arg>         message amout | default 100
 -h,--help                Print help
 -n,--namesrvAddr <arg>   Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -s,--size <arg>          message size | default 128 Byte
 -t,--topic <arg>         topic name


usage: mqadmin clusterRT [-a <arg>] [-c <arg>] [-h] [-i <arg>] [-m <arg>] [-n <arg>] [-p <arg>] -s <arg>
 -a,--amout <arg>          message amout | default 100
 -c,--cluster <arg>        cluster name | default display all cluster
 -h,--help                 Print help
 -i,--interval <arg>       print interval | default 10 seconds
 -m,--machine room <arg>   machine room name | default noname
 -n,--namesrvAddr <arg>    Name server address list, eg: 192.168.0.1:9876;192.168.0.2:9876
 -p,--print log <arg>      print as tlog | default false
 -s,--size <arg>           message size | default 128 Byte


```
